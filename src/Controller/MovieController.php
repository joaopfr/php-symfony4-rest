<?php

namespace App\Controller;



use App\Entity\Movie;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

class MovieController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/movies", name="get_movies")
     */
    public function movies()
    {
        $em = $this->getDoctrine()->getManager();
        $movies = $em->getRepository(Movie::class)->findAll();
        return $this->json(["data" => $movies]);
    }

    /**
     * @param Movie $movie
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Rest\Get("/movies/{id}", name="get_movie")
     */
    public function movie(Movie $movie)
    {
        return $this->json($movie);
    }
}
