<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MovieRepository")
 * @ApiResource(attributes={"pagination_client_enabled"=true, "pagination_items_per_page"=10, "pagination_client_items_per_page"=true, "normalization_context"={"groups"={"movies"}}, "force_eager"=false})
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "title": "partial", "description": "partial", "category": "exact"})
 */
class Movie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups("movies")
     */
    private $id;

    /**
     * @var mixed
     *
     * @ORM\Column(type="string")
     *
     * @Groups("movies")
     */
    private $title;

    /**
     * @var mixed
     *
     * @ORM\Column(type="text")
     *
     * @Groups("movies")
     */
    private $description;

    /**
     * @var mixed
     *
     * @ORM\Column(type="date")
     *
     * @Groups("movies")
     */
    private $release_date;

    /**
     * @var mixed
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="movies")
     *
     * @Groups("movies")
     */
    private $category;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Movie
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Movie
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReleaseDate()
    {
        return $this->release_date;
    }

    /**
     * @param mixed $release_date
     * @return Movie
     */
    public function setReleaseDate(\DateTime $release_date)
    {
        $this->release_date = $release_date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return Movie
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }
}
