<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 * @ApiResource(attributes={"normalization_context"={"groups"={"movies"}}})
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups("movies")
     */
    private $id;

    /**
     * @var mixed
     *
     * @ORM\Column(type="string")
     *
     * @Groups("movies")
     */
    private $name;

    /**
     * @var mixed
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Movie", mappedBy="category")
     *
     * @Groups("movies")
     */
    private $movies;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMovies()
    {
        return $this->movies;
    }

    /**
     * @param mixed $movies
     * @return Category
     */
    public function setMovies($movies)
    {
        $this->movies = $movies;
        return $this;
    }
}
